console.log('page load - entered main.js for js-other api');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo(){
    console.log('entered getFormInfo!');
    // call displayinfo
    var type = document.getElementById("name-text").value;
    console.log('Type you entered is ' + type);
    makeNetworkCallToPokeApi(type);

} // end of get form info

function makeNetworkCallToPokeApi(type){
    console.log('entered make nw call' + type);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://pokeapi.co/api/v2/type/" + type;
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        //console.log(xhr.responseText);
        // do something
        updatePokeWithResponse(type, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request

} // end of make nw call

function updatePokeWithResponse(type, response_text){
    var response_json = JSON.parse(response_text);
    // update a label
    var label1 = document.getElementById("response-line1");
    console.log(response_json['pokemon'])
    console.log(response_json['pokemon']['0']);
    console.log(response_json['pokemon']['0']['pokemon']);
    console.log("hi");
    console.log(response_json['pokemon']['0']['pokemon']['name']);
    var name = response_json['pokemon']['0']['pokemon']['name'];
    console.log(name);
    label1.innerHTML =  'your ' + type + ' type pokemon is ' + name;
    makeNetworkCallToGenderApi(name);

} // end of updateAgeWithResponse

function makeNetworkCallToGenderApi(name){
    console.log('entered make nw call' + name);
    // set up url
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    var url = "https://api.genderize.io/?name=" + name;
    xhr.open("GET", url, true); // 2 - associates request attributes with xhr

    // set up onload
    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        console.log(xhr.responseText);
        // do something
        updateGenderWithResponse(name, xhr.responseText);
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request
    //updateGenderWithResponse(gender, response_text)
} // end of make nw call

function updateGenderWithResponse(name, response_text){
    var response_json = JSON.parse(response_text);
    // update a label
    var label1 = document.getElementById("response-line2");

    if(response_json['gender'] == null){
        label1.innerHTML = 'Apologies, we could not find your pokemon\'s gender.'
    } else{
        label1.innerHTML =    'your pokemon\'s gender is ' + response_json['gender'];
        var age = parseInt(response_json['gender']);
    }
} // end of updateAgeWithResponse



